const express = require("express");
const router = express.Router()

const TaskController = require('../controllers/taskController')

router.get("/", (req, res) => {
	TaskController.getAllTask().then(resultFromConroller => res.send(resultFromConroller))
})

router.post("/", (req, res) => {
	TaskController.createTask(req.body).then(result => res.send(result))
})


router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result));
})

router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})


// Activity
// creating a route for retreiving a specific task
router.get("/:id", (req, res) => {
	TaskController.specificTask(req.params.id).then(result => res.send(result))
})

//  creating a route for updating the status for a specific task
router.put("/:id/complete", (req, res) => {
	TaskController.statusTask(req.params.id, req.body).then(result => res.send(result))
})

module.exports = router;