const Task = require('../models/task')

module.exports.getAllTask = () =>{
	return Task.find({}).then( result => {
		return result;
	})
}


module.exports.createTask = (requestBody) =>{
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}
		result.name = newContent.name
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})
}

//Activity 

// retrieve a specific task using GET method
module.exports.specificTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return result
		}
	})
}


// update a status on a specific task using PUT method
module.exports.statusTask = (taskId, updateStatus) =>{
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}
		result.status = updateStatus.status
		return result.save().then((updatedStatus, saveErr) => {
			if (saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedStatus
			}
		})
	})
}